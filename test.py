import requests
import json

def fetch_data_from_api(api_url):
    try:
        # Make a GET request to the API endpoint
        response = requests.get(api_url)

        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            # Parse the JSON response
            data = response.json()
            return data
        else:
            print(f"Error: Unable to fetch data from API. Status Code: {response.status_code}")
            return None

    except Exception as e:
        print(f"Error: {e}")
        return None

def generate_json_output(api_url):
    # Fetch data from the API
    data = fetch_data_from_api(api_url)

    if data:
        try:
            # Print the JSON data to stdout
            print(json.dumps(data, indent=2))
        except Exception as e:
            print(f"Error: Unable to print JSON data. {e}")

if __name__ == "__main__":
    # Use the JSONPlaceholder API as an example
    api_url = 'https://jsonplaceholder.typicode.com/todos'

    generate_json_output(api_url)
